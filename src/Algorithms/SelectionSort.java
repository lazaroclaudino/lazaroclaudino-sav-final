package Algorithms;

import javax.swing.*;
import GUI.GUI;

public class SelectionSort implements SortAlgorithm {

    long startTime = System.currentTimeMillis() / 1000;

    public void sort(GUI gui, int[] arr) {
        int n = arr.length;

        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;

            for (int j = i + 1; j < n; j++) {
                if ((gui.getOrder().equals("AZ") && arr[j] < arr[minIndex]) || (gui.getOrder().equals("ZA") && arr[j] > arr[minIndex])) {
                    minIndex = j;
                }
            }

            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;

            final int currentIteration = i;
            SwingUtilities.invokeLater(() -> {
                gui.updateBars(arr);
                gui.updateCount(currentIteration);
            });

            try {
                Thread.sleep(gui.getSpeed());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        long endTime = System.currentTimeMillis() / 1000;
        long totalTime = endTime - startTime;
        String formattedTime = String.format("%d seconds", totalTime);
        gui.updateTime(formattedTime);
    }
}

package Algorithms;
import GUI.GUI;

import javax.swing.*;

public class BubbleSort implements SortAlgorithm {

    long startTime = System.currentTimeMillis() / 1000;

    public void sort(GUI gui, int[] arr) {
        int n = arr.length;
        boolean swapped;

        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) {
                boolean shouldSwap = false;

                if (gui.getOrder().equals("AZ")) {
                    shouldSwap = arr[j] > arr[j + 1];
                } else if (gui.getOrder().equals("ZA")) {
                    shouldSwap = arr[j] < arr[j + 1];
                }

                if (shouldSwap) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;

                    final int currentIteration = i + 1;
                    SwingUtilities.invokeLater(() -> {
                        gui.updateBars(arr);
                        gui.updateCount(currentIteration);
                    });

                    try {
                        Thread.sleep(gui.getSpeed());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }

        long endTime = System.currentTimeMillis() / 1000;
        long totalTime = endTime - startTime;
        String formattedTime = String.format("%d seconds", totalTime);
        gui.updateTime(formattedTime);
    }
}

package User;

public class Args {
    private String type;
    private int algorithm;
    private String order;
    private int[] values;
    private int speed;
    private String inputType;

    public Args(String[] args) {
        parseArgs(args);
    }

    private void parseArgs(String[] args) {
        for (String arg : args) {
            String[] parts = arg.split("=");
            if (parts.length != 2) {
                System.out.println("Invalid argument: " + arg);
                continue;
            }

            String key = parts[0].trim();
            String value = parts[1].trim();

            parseArg(key, value);
        }
    }

    private void parseArg(String key, String value) {
        switch (key) {
            case "t" -> parseType(value);
            case "a" -> parseAlgorithm(value);
            case "o" -> parseOrder(value);
            case "in" -> parseInputType(value);
            case "v" -> parseValues(value);
            case "s" -> parseSpeed(value);
            default -> System.out.println("Invalid argument format: " + key);
        }
    }

    private void parseType(String value) {
        if (value.equals("n") || value.equals("c")) {
            type = value;
        } else {
            System.out.println("Invalid type (try c or n): " + value);
        }
    }

    private void parseAlgorithm(String value) {
        try {
            algorithm = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println("Invalid algorithm (try 1,2 or 3): " + value);
        }
    }

    private void parseOrder(String value) {
        if (value.equals("ZA") || value.equals("AZ")) {
            order = value;
        } else {
            System.out.println("Invalid order, try (AZ or ZA) " + value);
        }
    }

    private void parseInputType(String value) {
        if (value.equals("m") || value.equals("r")) {
            inputType = value;
        } else {
            System.out.println("Invalid input (try r or m)de " + value);
        }
    }

    private void parseValues(String value) {
        if (type != null) {
            String[] valueStrings = value.split(",");
            values = new int[valueStrings.length];
            try {
                for (int i = 0; i < valueStrings.length; i++) {
                    if (type.equals("n")) {
                        values[i] = Integer.parseInt(valueStrings[i]);
                    } else if (type.equals("c")) {
                        char letter = valueStrings[i].trim().charAt(0);
                        values[i] = letter;
                    }
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid value: " + value);
            }
        }
    }

    private void parseSpeed(String value) {
        try {
            speed = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println("Invalid speed: " + value);
        }
    }

    public String getType() {
        return type;
    }

    public int getAlgorithm() {
        return algorithm;
    }

    public String getOrder() {
        return order;
    }

    public int[] getValues() {
        return values;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isRandomInput() {
        return "r".equals(inputType);
    }
}

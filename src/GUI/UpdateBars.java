package GUI;

import User.Args;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Random;

public class UpdateBars {

    private final GUI gui;
    private final Args args;

    public UpdateBars(GUI gui, Args args) {
        this.gui = gui;
        this.args = args;
    }

    public void update(int[] arr) {
        clearBarsPanel();
        setBarsPanelLayout();

        int maxValue = getMaxValue(arr);

        Random random = new Random();

        for (int value : arr) {
            JPanel barAndLabelPanel = createBarAndLabelPanel(value, maxValue, random);
            gui.barsPanel.add(barAndLabelPanel);
        }

        repaintFrame();
    }

    private void clearBarsPanel() {
        gui.barsPanel.removeAll();
    }

    private void setBarsPanelLayout() {
        gui.barsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
    }

    private int getMaxValue(int[] arr) {
        return args.isRandomInput() ? 1000 : Arrays.stream(arr).max().orElse(Integer.MIN_VALUE);
    }

    private JPanel createBarAndLabelPanel(int value, int maxValue, Random random) {
        String valueToDisplay = getStringValue(value);

        int originalBarHeight = (int) (((double) value / maxValue) * (gui.frame.getHeight() - 100));
        int barHeight = (int) (originalBarHeight * 10.0);

        JPanel barAndLabelPanel = new JPanel(new BorderLayout());

        JPanel barPanel = createBarPanel(barHeight, random);

        JLabel label = new JLabel(valueToDisplay, SwingConstants.CENTER);

        barAndLabelPanel.add(barPanel, BorderLayout.SOUTH);
        barAndLabelPanel.add(label, BorderLayout.NORTH);

        return barAndLabelPanel;
    }

    private JPanel createBarPanel(int barHeight, Random random) {
        JPanel barPanel = new JPanel();
        barPanel.setPreferredSize(new Dimension(30, barHeight));
        barPanel.setBackground(new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
        return barPanel;
    }

    private String getStringValue(int value) {
        String valueToDisplay = "";

        if ("n".equals(args.getType())) {
            valueToDisplay = String.valueOf(value);
        } else if ("c".equals(args.getType()) && value >= 0 && value <= 127) {
            valueToDisplay = String.valueOf((char) value);
        }

        return valueToDisplay;
    }

    private void repaintFrame() {
        gui.frame.revalidate();
        gui.frame.repaint();
    }
}

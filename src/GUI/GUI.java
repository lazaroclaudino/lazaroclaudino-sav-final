package GUI;

import User.Args;

import javax.swing.*;
import java.awt.*;


public class GUI {
    final Args args;
    final JFrame frame;
    final JPanel barsPanel;
    private final JLabel iterationLabel;
    private final JLabel timeLabel;
    private ImageIcon bgImage = null;
    private final SoundPlayer soundPlayer;
    private final UpdateBars updateBars;


    public GUI(Args args) {
        this.args = args;
        updateBars = new UpdateBars(this, args);

        frame = new JFrame("Lázaro SAV");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1280, 720);

        soundPlayer = new SoundPlayer("soundtrack.wav");
        soundPlayer.play();

        try {
            bgImage = new ImageIcon("BG.jpg");
            frame.setIconImage(bgImage.getImage());
        } catch (Exception e) {
            System.err.println("Error in image: " + e.getMessage());
        }

        frame.setLayout(new BorderLayout());

        JPanel bgPanel = new JPanel() {
            public void paintComponent(Graphics g) {
                if (bgImage != null) {
                    g.drawImage(bgImage.getImage(), 0, 0, null);
                }
            }
        };
        bgPanel.setOpaque(false);
        frame.setContentPane(bgPanel);

        JPanel labelsPanel = new JPanel(new GridLayout(2, 1));

        iterationLabel = new JLabel("Iterations: 0", SwingConstants.CENTER);
        iterationLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        labelsPanel.add(iterationLabel);

        timeLabel = new JLabel("Time: 0 seconds", SwingConstants.CENTER);
        timeLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        labelsPanel.add(timeLabel);

        frame.add(labelsPanel, BorderLayout.NORTH);

        barsPanel = new JPanel();
        frame.add(barsPanel, BorderLayout.CENTER);

        frame.setVisible(true);
    }

    public void updateCount(int count) {
        iterationLabel.setText("Iterations: " + count);
    }

    public void updateTime(String time) {
        timeLabel.setText("Time: " + time);
    }

    public int getSpeed() {
        return args.getSpeed();
    }

    public String getOrder() {
        return args.getOrder();
    }
    public void updateBars(int[] arr) {

        updateBars.update(arr);

    }
}

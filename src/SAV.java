import Algorithms.BubbleSort;
import Algorithms.InsertionSort;
import Algorithms.SelectionSort;
import Algorithms.SortAlgorithm;
import GUI.GUI;
import User.Args;

import java.util.Random;

public class SAV {

    public static void main(String[] args) {
        Args argsObj = new Args(args);
        GUI gui = new GUI(argsObj);
        int[] arr = createInputArray(argsObj);

        if (arr == null) {
            System.out.println("Invalid Input!");
            return;
        }

        SortAlgorithm sortAlg = createSortAlgorithm(argsObj);

        if (sortAlg == null) {
            System.out.println("Invalid Algorithm!");
            return;
        }

        sortAlg.sort(gui, arr);
    }

    private static int[] createInputArray(Args argsObj) {
        if (argsObj.isRandomInput()) {
            if (argsObj.getType().equals("n")) {
                return generateRandomIntArray();
            } else if (argsObj.getType().equals("c")) {
                char[] charArray = generateRandomCharArray();
                int[] arr = new int[charArray.length];
                for (int i = 0; i < charArray.length; i++) {
                    arr[i] = charArray[i];
                }
                return arr;
            }
        } else {
            return argsObj.getValues();
        }
        return null;
    }

    private static SortAlgorithm createSortAlgorithm(Args argsObj) {
        return switch (argsObj.getAlgorithm()) {
            case 1 -> new BubbleSort();
            case 2 -> new InsertionSort();
            case 3 -> new SelectionSort();
            default -> null;
        };
    }

    private static int[] generateRandomIntArray() {
        int[] randomArray = new int[10];
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            randomArray[i] = random.nextInt((100 - 1) + 1) + 1;
        }

        return randomArray;
    }

    private static char generateRandomChar() {
        Random random = new Random();
        return (char) (random.nextInt(26) + 'a');
    }

    private static char[] generateRandomCharArray() {
        char[] randomChars = new char[10];

        for (int i = 0; i < 10; i++) {
            randomChars[i] = generateRandomChar();
        }

        return randomChars;
    }
}

# Projeto SAV - Lázaro Landerson

## Descrição

Essa é a minha implementação dos algoritmos de ordenação: BubbleSort, InsertionSort e SelectionSort.

- *BubbleSort*: é um algoritmo de ordenação simples que compara elementos adjacentes e troca os que estão em ordem errada.

- *InsertionSort*: funciona comparando cada elemento da lista com os elementos anteriores e movendo o elemento para a posição correta se ele estiver fora de ordem.

- *SelectionSort*: é um algoritmo de ordenação que funciona comparando os elementos da lista e selecionando o menor elemento em cada iteração. O menor elemento é então movido para o início da lista, e o processo é repetido até que todos os elementos estejam ordenados.

## Como executar o programa

1. Certifique-se de ter o Java instalado em sua máquina.
2. Clone este repositório para o seu computador.
3. Navegue até o diretório do projeto.
4. Compile o programa utilizando o comando: `javac SAV.java`
5. Veja o exemplo de execução
## Funcionamento do programa

Após executar o programa com os valores desejados, ele solicitará que você escolha qual algoritmo de ordenação deseja utilizar para ordenar os valores fornecidos. Caso insira uma letra em vez de um número correspondente ao algoritmo, ocorrerá um erro.

## Exemplo de execução

java SAV t=c a=1 o=AZ in=m v="z,a,c,d,e,f,g" s=100

- `TIPO`: Escolha o tipo de entrada (use `n` para NUMÉRICO ou `c` para CARACTERE).
- `ALGORITMO`: Escolha o algoritmo de ordenação (use `1` para Bubble Sort, `2` para Selection Sort ou `3` para Insertion Sort).
- `ORDEM`: Especifique a ordem da ordenação (use `AZ` para CRESCENTE ou `ZA` para DECRESCENTE).
- `ENTRADA`: Escolha o método de entrada (use `m` para MANUAL ou `r` para RANDÔMICO).
- `VALORES`: Fornecer o vetor completo a ser ordenado (por exemplo, `1,2,3,4,5,6` ou `a,b,c,d,e,d,a,d`).
- `VELOCIDADE`: Defina a velocidade de ordenação em milissegundos.


## Exemplos de execução:

- Execução 1: Caractere, Crescente, BubbleSort, lenta e Manual
java SAV t=c a=1 o=AZ in=m v="z,d,e,h,m,f,g,p" s=600

- Execução 2: Caractere, Decrescente, InsertionSort, muito rápida e Manual
java SAV t=c a=2 o=ZA in=m v="h,j,z,m,d,c,b,a" s=100

- Execução 3: Número Inteiro, Crescente, SelectionSort, rápida e Aleatório
java SAV t=n a=3 o=AZ in=r v="1,2" s=250

- Execução 4: Número Inteiro, Decrescente, InsertionSort, muito rápida e Aleatório
java SAV t=n a=2 o=ZA in=r v="20,15,10,5,1" s=75

- Execução 5: Caractere, Aleatório, BubbleSort, rápida e Aleatório
java SAV t=c a=1 o=AZ in=r v="a,b" s=150
